---
layout: markdown_page
title: "Marketing"
---
[Developer marketing](/handbook/marketing/developer-marketing)
[Demand generation](/handbook/marketing/demand-generation)
[Social media](/handbook/marketing/developer-marketing/social-media/)

## Developer Relations Organization (dev rel org)

- [technical writing](https://about.gitlab.com/jobs/technical-writer/)
- [developer advocacy](https://about.gitlab.com/jobs/developer-advocate/)
- field marketing (events)
- [developer marketing](/handbook/marketing/developer-marketing)