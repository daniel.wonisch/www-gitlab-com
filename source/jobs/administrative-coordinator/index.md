---
layout: markdown_page
title: "Administrative Coordinator"
---

## Responsibilities


* Assist People Operations, specifically
   * Prepare contracts and help ensure smooth onboarding of new team members, with
   a focus on non-US based team members.
* Help organize team events and provide support for team travel.
* Assist in running and improving recruiting and hiring processes.
* Conduct screening calls as part of our [Hiring process](https://about.gitlab.com/handbook/hiring/#screening-call)
* Help executives and other team members with purchases, vendors, gifts, reservations,
and other miscellaneous tasks.
* Come up with (and execute) fun ideas that contribute to team spirit in a remote-first
and widely distributed company.
